package com.alchimie.mam.infra.jooq;

import com.alchimie.mam.infra.DemoMamInfraApplication;
import com.alchimie.mam.tables.pojos.Videos;
import org.jooq.DSLContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoMamInfraApplication.class)
public class QueryTest {
    @Autowired
    DSLContext dsl;

    @Test
    public void test() {
        com.alchimie.mam.tables.Videos v = com.alchimie.mam.tables.Videos.VIDEOS.as("v");
        Optional<Videos> videos = dsl.selectFrom(v).where(v.ID.eq(1L)).fetchOptionalInto(Videos.class);
        assertEquals(true, videos.isPresent());
        assertEquals("Dango1", videos.get().getTitle());
        assertEquals("Est un esclave affranchi qui devient un des plus grand chasseurs de tête.", videos.get().getDescription());
    }
}
