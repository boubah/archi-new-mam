package com.alchimie.mam.infra.jooq;

import com.alchimie.mam.infra.DemoMamInfraApplication;
import com.alchimie.mam.domain.media.IObtainMoviePort;
import com.alchimie.mam.domain.media.Movie;
import io.vavr.collection.List;
import io.vavr.control.Option;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoMamInfraApplication.class)
public class ObtainMovieApterTest {

    @Autowired
    IObtainMoviePort obtainMoviePort;

    @Test
    public void findOneSuccess() {
        Option<Movie> videos = obtainMoviePort.findOne(1L);
        assertTrue(videos.isDefined() && !videos.isEmpty());
        assertEquals("Dango1", videos.get().getMovieTitle());
        assertEquals("Est un esclave affranchi qui devient un des plus grand chasseurs de tête.", videos.get().getMovieDescription());
    }


    @Test
    public void findOneFailure() {
        Option<Movie> videos = obtainMoviePort.findOne(4L);
        assertFalse(videos.isDefined() && videos.isEmpty());
    }

    @Test
    public void findAllByTitle() {
        List<Movie> videosList = obtainMoviePort.findAll(Option.of("Dango1"), Option.none(), Option.none(), Option.none(), Option.none(), Option.none(), Option.none());
        Long expectDuration = 120L;
        assertEquals(1,videosList.size());
        assertEquals("Dango1", videosList.get(0).getMovieTitle());
        assertEquals("Est un esclave affranchi qui devient un des plus grand chasseurs de tête.", videosList.get(0).getMovieDescription());
        assertEquals(expectDuration, videosList.get(0).getMovieDuration());
        assertEquals("4k", videosList.get(0).getMovieFormat());
        assertEquals("james Fox", videosList.get(0).getMovieActors());
        assertEquals("image1.png", videosList.get(0).getMovieCovers());
        assertEquals("Film", videosList.get(0).getMovieType());
    }

    @Test
    public void findAllByDescription() {
        List<Movie> videosList = obtainMoviePort.findAll(Option.none(), Option.of("Est un esclave affranchi qui devient un des plus grand chasseurs de tête."), Option.none(), Option.none(), Option.none(), Option.none(), Option.none());
        assertEquals(3,videosList.size());
        assertEquals("Dango1", videosList.get(0).getMovieTitle());
        assertEquals("Est un esclave affranchi qui devient un des plus grand chasseurs de tête.", videosList.get(0).getMovieDescription());
        assertEquals("Film", videosList.get(0).getMovieType());
    }

    @Test
    public void findAllByType() {
        List<Movie> videosList = obtainMoviePort.findAll(Option.none(), Option.none(), Option.of("Film"), Option.none(), Option.none(), Option.none(), Option.none());
        assertEquals(3,videosList.size());
        assertEquals("Dango1", videosList.get(0).getMovieTitle());
        assertEquals("Est un esclave affranchi qui devient un des plus grand chasseurs de tête.", videosList.get(0).getMovieDescription());
        assertEquals("Film", videosList.get(0).getMovieType());
    }
    @Test
    public void findAllByDuration() {
        List<Movie> videosList = obtainMoviePort.findAll(Option.none(), Option.none(), Option.none(), Option.of(120L), Option.none(), Option.none(), Option.none());
        assertEquals(2,videosList.size());
        assertEquals("Dango1", videosList.get(0).getMovieTitle());
        assertEquals("Est un esclave affranchi qui devient un des plus grand chasseurs de tête.", videosList.get(0).getMovieDescription());
        assertEquals("Film", videosList.get(0).getMovieType());
    }

    @Test
    public void findAllByFormat() {
        List<Movie> videosList = obtainMoviePort.findAll(Option.none(), Option.none(), Option.none(), Option.none(), Option.of("4k"), Option.none(), Option.none());
        assertEquals(3,videosList.size());
        assertEquals("Dango1", videosList.get(0).getMovieTitle());
        assertEquals("Est un esclave affranchi qui devient un des plus grand chasseurs de tête.", videosList.get(0).getMovieDescription());
        assertEquals("Film", videosList.get(0).getMovieType());
    }

}
