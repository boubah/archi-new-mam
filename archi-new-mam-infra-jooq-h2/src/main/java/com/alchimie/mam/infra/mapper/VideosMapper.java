package com.alchimie.mam.infra.mapper;

import com.alchimie.mam.domain.media.Movie;
import com.alchimie.mam.tables.pojos.Videos;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface VideosMapper {
    @Mappings({
            @Mapping(source = "title", target = "movieTitle"),
            @Mapping(source = "description", target = "movieDescription"),
            @Mapping(source = "type", target = "movieType"),
            @Mapping(source = "duration", target = "movieDuration"),
            @Mapping(source = "format", target = "movieFormat"),
            @Mapping(source = "actors", target = "movieActors"),
            @Mapping(source = "covers", target = "movieCovers")
    })
    Movie videosToMovie(Videos videos);
}
