package com.alchimie.mam.infra;

import com.alchimie.mam.domain.media.IObtainMoviePort;
import com.alchimie.mam.infra.Jooq.ObtainMovieJooQApter;
import com.alchimie.mam.infra.mapper.VideosMapper;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;

@SpringBootApplication
public class DemoMamInfraApplication {
    public static void main(String... args) {
        SpringApplication.run(DemoMamInfraApplication.class, args);
    }

    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.driverClassName}")
    private String driverClassName;
    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String password;

    @Bean
    public DataSource dataSource() {
        return DataSourceBuilder.create()
                .url(url)
                .username(username)
                .driverClassName(driverClassName)
                .password(password)
                .build();
    }

    @Bean
    @Transactional
    public IObtainMoviePort videosPort(DSLContext dslContext, VideosMapper videosMapper) {
        return new ObtainMovieJooQApter(dslContext, videosMapper);
    }

}
