package com.alchimie.mam.infra.Jooq;

import com.alchimie.mam.domain.media.IObtainMoviePort;
import com.alchimie.mam.domain.media.Movie;
import com.alchimie.mam.infra.mapper.VideosMapper;
import com.alchimie.mam.tables.pojos.Videos;
import io.vavr.collection.List;
import io.vavr.control.Option;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.SelectWhereStep;
import org.jooq.impl.DSL;

import static com.alchimie.mam.tables.Videos.VIDEOS;


public class ObtainMovieJooQApter implements IObtainMoviePort {

    private DSLContext dsl;

    private VideosMapper videosMapper;
    public ObtainMovieJooQApter(DSLContext dsl, VideosMapper videosMapper) {
        this.dsl = dsl;
        this.videosMapper = videosMapper;
    }

    @Override
    public Option<Movie> findOne(Long idVideos) {
        Option<Videos> videos = Option.ofOptional(dsl.selectFrom(VIDEOS)
                    .where(VIDEOS.ID.eq(idVideos))
                    .fetchOptionalInto(Videos.class));
        return videos.map(videosMapper::videosToMovie);
    }

    @Override
    public List<Movie> findAll(Option<String> title, Option<String> description, Option<String> type,
                                Option<Long> duration, Option<String> format, Option<String> actors, Option<String> covers) {
        java.util.List<Condition> conditions = JooqUtils.search(title, description, type, duration, format, actors, covers);
        com.alchimie.mam.tables.Videos v = VIDEOS.as("v");
        SelectWhereStep selectWhereStep =  dsl.selectFrom(v);
        List<Videos> videosList = List.ofAll((conditions.isEmpty() ? selectWhereStep : selectWhereStep
                .where(DSL.or(conditions))).fetchInto(Videos.class));

        return videosList.map(videosMapper::videosToMovie);
    }
}
