package com.alchimie.mam.infra.Jooq;

import io.vavr.control.Option;
import org.jooq.Condition;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

import static com.alchimie.mam.tables.Videos.VIDEOS;

public class JooqUtils {

    private static Predicate<Option> emptyCriteria = Option::isEmpty;
    private static BiConsumer<List<Condition>, Condition> addCondition = (conditions, condition) -> conditions.add(condition);

    public static List<Condition> search(Option<String> title,
                                         Option<String> description,
                                         Option<String> type,
                                         Option<Long> duration,
                                         Option<String> format,
                                         Option<String> actors,
                                         Option<String> covers) {

        List<Condition> conditions = new ArrayList<>();
        com.alchimie.mam.tables.Videos v = VIDEOS.as("v");

        if (JooqUtils.emptyCriteria.negate().test(title)) {
            JooqUtils.addCondition.accept(conditions, v.TITLE.eq(title.get()));
        }

        if (JooqUtils.emptyCriteria.negate().test(description)) {
            JooqUtils.addCondition.accept(conditions, v.DESCRIPTION.eq(description.get()));
        }

        if (JooqUtils.emptyCriteria.negate().test(type)) {
            JooqUtils.addCondition.accept(conditions, v.TYPE.eq(type.get()));
        }

        if (JooqUtils.emptyCriteria.negate().test(duration)) {
            JooqUtils.addCondition.accept(conditions, v.DURATION.eq(duration.get()));
        }

        if (JooqUtils.emptyCriteria.negate().test(format)) {
            JooqUtils.addCondition.accept(conditions, v.FORMAT.eq(format.get()));
        }

        if (JooqUtils.emptyCriteria.negate().test(actors)) {
            JooqUtils.addCondition.accept(conditions, v.ACTORS.eq(actors.get()));
        }

        if (JooqUtils.emptyCriteria.negate().test(covers)) {
            JooqUtils.addCondition.accept(conditions, v.COVERS.eq(covers.get()));
        }

        return conditions;
    }
}
