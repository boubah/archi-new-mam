package com.alchimie.mam.infra.mapper;

import com.alchimie.mam.domain.media.Movie;
import com.alchimie.mam.tables.pojos.Videos;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor"
)
@Component
public class VideosMapperImpl implements VideosMapper {

    @Override
    public Movie videosToMovie(Videos videos) {
        if ( videos == null ) {
            return null;
        }

        Movie movie = new Movie();

        movie.setMovieDescription( videos.getDescription() );
        movie.setMovieCovers( videos.getCovers() );
        movie.setMovieType( videos.getType() );
        movie.setMovieActors( videos.getActors() );
        movie.setMovieTitle( videos.getTitle() );
        movie.setMovieDuration( videos.getDuration() );
        movie.setMovieFormat( videos.getFormat() );

        return movie;
    }
}
