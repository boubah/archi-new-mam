DROP TABLE IF EXISTS VIDEOS;

CREATE TABLE VIDEOS (
  id bigint(11) NOT NULL AUTO_INCREMENT,
  title varchar (200) NOT NULL,
  description varchar(200) NOT NULL,
  type varchar(200) NOT NULL,
  duration bigint(11) NOT NULL,
  format varchar (200) NOT NULL,
  actors varchar (255) NOT NULL,
  covers varchar (20) DEFAULT '',
  created_on datetime DEFAULT NULL,
  updated_on datetime,
  PRIMARY KEY (ID)
);