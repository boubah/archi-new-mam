package com.alchimie.mam.domain.media;

public class Movie {
    private String movieTitle;
    private String movieDescription;
    private String movieType;
    private Long movieDuration;
    private String movieFormat;
    private String movieActors;
    private String movieCovers;

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getMovieDescription() {
        return movieDescription;
    }

    public void setMovieDescription(String movieDescription) {
        this.movieDescription = movieDescription;
    }

    public String getMovieType() {
        return movieType;
    }

    public void setMovieType(String movieType) {
        this.movieType = movieType;
    }

    public Long getMovieDuration() {
        return movieDuration;
    }

    public void setMovieDuration(Long movieDuration) {
        this.movieDuration = movieDuration;
    }

    public String getMovieFormat() {
        return movieFormat;
    }

    public void setMovieFormat(String movieFormat) {
        this.movieFormat = movieFormat;
    }

    public String getMovieActors() {
        return movieActors;
    }

    public void setMovieActors(String movieActors) {
        this.movieActors = movieActors;
    }

    public String getMovieCovers() {
        return movieCovers;
    }

    public void setMovieCovers(String movieCovers) {
        this.movieCovers = movieCovers;
    }
}
