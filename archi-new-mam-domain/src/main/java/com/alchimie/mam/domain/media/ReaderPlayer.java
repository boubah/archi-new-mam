package com.alchimie.mam.domain.media;

import io.vavr.collection.List;
import io.vavr.control.Option;

public class ReaderPlayer implements IPlayerRequestPort {

    private IObtainMoviePort obtainMoviePort;

    public ReaderPlayer(IObtainMoviePort obtainMoviePort) {
        this.obtainMoviePort = obtainMoviePort;
    }

    @Override
    public Movie playMovie(Long idMovie) {
        return obtainMoviePort.findOne(idMovie).get();
    }

    @Override
    public List<Movie> playMovies(Option<String> movieTitle,
                                  Option<String> movieDescription,
                                  Option<String> movieType,
                                  Option<Long> movieDuration,
                                  Option<String> movieFormat,
                                  Option<String> movieActors,
                                  Option<String> movieCovers) {

        List<Movie> movies = getMovies(movieTitle,movieDescription,
                movieType, movieDuration, movieFormat,
                movieActors, movieCovers);

        return movies;
    }

    private List<Movie> getMovies(Option<String> movieTitle,
                                  Option<String> movieDescription,
                                  Option<String> movieType,
                                  Option<Long> movieDuration,
                                  Option<String> movieFormat,
                                  Option<String> movieActors,
                                  Option<String> movieCovers) {

        return obtainMoviePort.findAll(movieTitle,movieDescription,
                movieType, movieDuration, movieFormat,
                movieActors, movieCovers);
    }
}
