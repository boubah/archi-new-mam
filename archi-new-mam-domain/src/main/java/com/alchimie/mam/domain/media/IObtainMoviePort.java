package com.alchimie.mam.domain.media;

import io.vavr.collection.List;
import io.vavr.control.Option;

public interface IObtainMoviePort {
    Option<Movie> findOne(Long idVideos);
    List<Movie> findAll(Option<String> movieTitle,
                        Option<String> movieDescription,
                        Option<String> movieType,
                        Option<Long> movieDuration,
                        Option<String> movieFormat,
                        Option<String> movieActors,
                        Option<String> movieCovers);
}
