package com.alchimie.mam.domain.media;

import io.vavr.collection.List;
import io.vavr.control.Option;

public interface IPlayerRequestPort {
    Movie playMovie(Long movieId);
    List<Movie> playMovies(Option<String> movieTitle,
                              Option<String> movieDescription,
                              Option<String> movieType,
                              Option<Long> movieDuration,
                              Option<String> movieFormat,
                              Option<String> movieActors,
                              Option<String> moviesCovers);
}
